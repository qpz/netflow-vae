import numpy as np
import pandas as pd
from generate_feature_file import generate_feature_file


paths = ["data/mar20_sample.csv"]
feature_id = "port_distribution"

generate_feature_file(paths, feature_id, write_path="tmp", feature_filename="features.csv")


