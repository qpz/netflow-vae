import tensorflow as tf 
import numpy as np 
import cvae
import matplotlib.pyplot as plt


vae_params = {'n_features': 2,
              'n_conditions': 0,
              'latent_dim': 3,
              'hidden_neurons': [100,100,200],
              'batch_norm_layer': [True, False, False, False, False, False],
              'n_hidden_layers': 3,
              'nz_samples': 10}


def get_minibatch(mb_size):
    # return minibatch: X_mb (input/features), Y_mb (conditional variables)
    x1 = np.random.randn(mb_size) * 10.0 + 0.0
    x2 = np.random.randn(mb_size) * 20.0 + 200.0

    X_mb = np.concatenate([x1.reshape(-1,1), x2.reshape(-1,1)], axis=1)

    return X_mb



n_iter = 20000
vae_params['batch_norm_layer'] = [True, False, False, False, False, False]

cvae_model = cvae.CVAE(vae_params)

losses = cvae_model.train(get_minibatch, n_iter=n_iter, mbsize=100, network_savename='./cvae_params', verbose=False)
print("Statistics of loss of the last 1000 iterations:")
print(np.mean(losses[(n_iter-1000):]))
print(np.std(losses[(n_iter-1000):]))


sample_xs = cvae_model.sample_X(1000, network_savename='./cvae_params')

plt.scatter(sample_xs[:,0], sample_xs[:,1])
plt.title("Generated X with trained VAE")
plt.show()


X = get_minibatch(1000)

plt.scatter(X[:,0], X[:,1])
plt.title("Groundtruth X")
plt.show()


recon_loss, kl_loss, gvae = cvae_model.get_errorloss_and_gradient(X, Y=None, batch_size=1, n_samples=10, network_savename='./cvae_params')

