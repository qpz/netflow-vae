import tensorflow as tf
import numpy as np
import sys


class CVAE(object):

    @staticmethod
    def _fillparams(current_params, new_params, required_keys=[]):

        for key in required_keys:
            if key not in new_params:
                raise Exception("Require CVAE parameter: {}".format(required_keys))

        for key in new_params.keys():
            if key in current_params:
                current_params[key] = new_params[key]
            else:
                raise Exception('Unknown CVAE parameter: {}'.format(key))
        return current_params


    def __init__(self, vae_params, weight_decay = 0.01, tfconfig = None, tftype=tf.float32):
        # tf.ConfigProto(device_count={'GPU': 1})#, gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.7))
        self.weight_decay = weight_decay
        self.tfconfig = tfconfig
        self.tftype = tftype

        self.params = {'n_features': 2,
                    'n_conditions': 1,
                    'latent_dim': 3,
                    'hidden_neurons': [100,100,100],
                    'n_hidden_layers': 3,
                    'batch_norm_layer': ['True', 'False', 'False', 'False', 'False', 'False'],
                    'nz_samples': 10}

        self.params = CVAE._fillparams(self.params, vae_params, required_keys=['n_features', 'n_conditions'])

        self._cvae = None


    @staticmethod
    def _make_activation_layer(x, activation):
        if activation == 'relu':
            network = tf.nn.relu(x)
        elif activation == 'leaky_relu':
            network = tf.nn.leaky_relu(x, alpha=0.2)
        elif activation == 'sigmoid':
            network = tf.nn.sigmoid(x)
        else:
            raise Exception("Only allow relu, sigmoid. Unknown activation: {}".format(activation))
        
        return network


    def _dense_batch(self, x, phase, scope, activation, n_hidden_neurons, use_batchnorm=False, reuse=False):
        # activation in {'relu', 'leaky_relu', 'sigmoid'}
        with tf.variable_scope(scope, reuse=reuse):

            if use_batchnorm:
                batch_norm_layer = tf.layers.batch_normalization(x, 
                                                training=phase,
                                                reuse=reuse)
                h1 = tf.contrib.layers.fully_connected(batch_norm_layer, n_hidden_neurons, 
                                                    activation_fn=None,
                                                    scope='{}_dense'.format(scope),
                                                    reuse=reuse,
                                                    weights_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay))

                network = CVAE._make_activation_layer(h1, activation)

            else:
                h1 = tf.contrib.layers.fully_connected(x, n_hidden_neurons, 
                                                    activation_fn=None,
                                                    scope='{}_dense'.format(scope),
                                                    reuse=reuse,
                                                    weights_regularizer=tf.contrib.layers.l2_regularizer(scale=self.weight_decay))
                network = CVAE._make_activation_layer(h1, activation)

        return network


    def __str__(self):
        description = ""
        description += "Number of features: {}, of conditions {}\n".format(self.params['n_features'], self.params['n_conditions'])
        description += "Dimension of latent variable: {}\n".format(self.params['latent_dim'])
        description += "CVAE structure:\n"
        description += "|  Input\n"
        description += "|  (Encoder)\n"
        description += "{}\n".format(self.params['hidden_neurons'])
        description += "|  (Decoder)\n"
        description += "{}\n".format( list(reversed(self.params['hidden_neurons'])) )
        description += "-----------\n"
        description += "Batchnorm: {}\n".format(self.params['batch_norm_layer'])
        description += "Number of sampled latent z per iteration: {}\n".format(self.params['nz_samples'])
        return description


    def cvae(self):

        if self._cvae is not None:
            return self._cvae

        # get_minibatch: a function returning mini-batch of data X, and conditional variables Y
        tf.reset_default_graph()

        n_conditions = self.params['n_conditions']
        n_features = self.params['n_features']

        z_dim = self.params['latent_dim']
        X_dim = n_features
        y_dim = n_conditions if n_conditions > 0 else 1
        h_dim = self.params['hidden_neurons']
        n_hidden = self.params['n_hidden_layers']
        nz_samples = self.params['nz_samples']
        batchnorm_layers = self.params['batch_norm_layer']

        # =============================== Q(z|X) ======================================
        X = tf.placeholder(self.tftype, shape=[None, X_dim], name='X')
        c = tf.placeholder(self.tftype, shape=[None, y_dim], name='c')
        z = tf.placeholder(self.tftype, shape=[None, z_dim], name='z')
        phase = tf.placeholder(tf.bool, name='phase')

        def Q(X, c):
            inputs = tf.concat(axis=1, values=[X, c])

            # mean
            first_layer = self._dense_batch(inputs, phase, scope='Q_mu_layer_0', activation='leaky_relu', 
                                n_hidden_neurons=h_dim[0], use_batchnorm=batchnorm_layers[0])
            hidden = first_layer
            for i in range(1, n_hidden):
                hidden = self._dense_batch(hidden, phase, scope='Q_mu_layer_{}'.format(i), activation='leaky_relu', 
                                    n_hidden_neurons=h_dim[i], use_batchnorm=batchnorm_layers[i])

            z_mu = tf.layers.dense(hidden, z_dim, activation=None, 
                                    kernel_initializer=tf.contrib.layers.xavier_initializer())

            # variance
            hidden = first_layer
            for i in range(1, n_hidden):
                hidden = self._dense_batch(hidden, phase, scope='Q_var_layer_{}'.format(i), activation='leaky_relu', n_hidden_neurons=h_dim[i], use_batchnorm=batchnorm_layers[i])

            z_var = tf.abs( tf.layers.dense(hidden, z_dim, activation=None,
                                    kernel_initializer=tf.contrib.layers.xavier_initializer()) )

            return z_mu, z_var


        def sample_z(mu, var):
            eps = tf.random_normal(shape=tf.shape(mu))
            return mu + tf.sqrt(var) * eps

        
        def sample_zs(mu, var, n=1):
            zs = []
            for _ in range(n):
                eps = tf.random_normal(shape=tf.shape(mu))
                zs.append( mu + tf.sqrt(var) * eps )
            return zs
        

        # =============================== P(X|z) ======================================

        def P(z, c, name, reuse=False):
            with tf.variable_scope('P', reuse=reuse):
                inputs = tf.concat(axis=1, values=[z,c], name='P_input')

                hidden = self._dense_batch(inputs, phase, scope='{}_P_layer_0'.format(name), activation='leaky_relu', n_hidden_neurons=h_dim[n_hidden-1], reuse=reuse, use_batchnorm=batchnorm_layers[n_hidden])

                for i in range(1, n_hidden):
                    hidden = self._dense_batch(hidden, phase, scope='{}_P_layer_{}'.format(name, i), activation='leaky_relu', n_hidden_neurons=h_dim[n_hidden-i-1], reuse=reuse, use_batchnorm=batchnorm_layers[i + n_hidden])

                x_reconstruct = tf.layers.dense(hidden, X_dim, activation=None, name='{}_x_reconstruct'.format(name), reuse=reuse)

            return x_reconstruct

        # =============================== TRAINING ====================================

        z_mu, z_var = Q(X, c)
        z_samples = sample_zs(z_mu, z_var, nz_samples)

        x_reconstruct = P(z_samples[0], c, name='training')
        recon_loss = tf.reduce_sum(tf.square(x_reconstruct - X),1)
            
        for i,z_sample in enumerate(z_samples[1:]):
            x_reconstruct = P(z_sample, c, name='training', reuse=True)
            recon_loss = recon_loss + tf.reduce_sum(tf.square(x_reconstruct - X),1)
        
        recon_loss = recon_loss / tf.cast(tf.constant(nz_samples), tf.float32)

        # D_KL(Q(z|X) || P(z)); calculate in closed form as both dist. are Gaussian
        kl_loss = 0.5 * tf.reduce_sum(z_var + z_mu**2 - 1. - tf.log(z_var), 1, name='kl_loss')
        # VAE loss
        reg_losses = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        vae_loss = tf.reduce_mean(recon_loss + kl_loss + tf.reduce_sum(reg_losses), name='vae_loss')

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        with tf.control_dependencies(update_ops):
            # Ensures that we execute the update_ops before performing the train_steps
            solver = tf.train.AdamOptimizer(0.001).minimize(vae_loss)


        grad_vaeloss_wrt_x = tf.add_n(tf.gradients(vae_loss, X))


        # sample X
        sample_x = P(z, c, name='training', reuse=True)

        self._cvae = {'X':X, 'c':c, 'z':z, 
                'z_mu': z_mu,
                'z_var': z_var,
                'is_training':phase, 
                'optimize': solver, 
                'sample_x': sample_x,
                'vae_loss': vae_loss, 
                'kl_loss': kl_loss, 
                'recon_loss': recon_loss,
                'grad_vaeloss_wrt_x': grad_vaeloss_wrt_x}
        
        return self._cvae


    def train(self, get_minibatch, n_iter, mbsize=10, network_savename=None, verbose=False):
        cvae = self.cvae()

        losses = np.zeros(n_iter)
        saver = tf.train.Saver()

        with tf.Session(config = self.tfconfig) as sess:
            sess.run(tf.global_variables_initializer())

            for i in range(n_iter):
                if self.params['n_conditions'] > 0:
                    X_mb, Y_mb = get_minibatch(mbsize)
                else:
                    X_mb = get_minibatch(mbsize)
                    Y_mb = np.ones([X_mb.shape[0], 1])

                _, loss = sess.run([cvae['optimize'], cvae['vae_loss']], 
                            feed_dict={cvae['is_training']: True, 
                                       cvae['X']: X_mb, 
                                       cvae['c']: Y_mb})
                losses[i] = loss
                
                if network_savename is not None:
                    saver.save(sess, network_savename, write_meta_graph=False)

                if verbose:
                    print('    Iter: {}:{}'.format(i, i*X_mb.shape[0]))
                    print('    Loss: {:.4}'. format(loss))
                    print()
                    sys.stdout.flush()

        return losses


    def sample_mean_Z(self, X, mbsize=10, network_savename=None, condition_var=None):
        if condition_var is None:
            condition_var = np.ones([X.shape[0],1])
        
        cvae = self.cvae()

        saver = tf.train.Saver()
        sample_mean_zs = []

        with tf.Session(config = self.tfconfig) as sess:
            sess.run(tf.global_variables_initializer())

            if network_savename is not None:
                saver.restore(sess, network_savename)
            
            for i in range(0, X.shape[0], mbsize):
                sample_mean_z = sess.run(cvae['z_mu'], feed_dict={cvae['X']: X, cvae['c']: condition_var, cvae['is_training']: False}) 
                sample_mean_zs.append(sample_mean_z)

        sample_mean_zs = np.concatenate(sample_mean_zs, axis=0)
        return sample_mean_zs


    def sample_X(self, n, mbsize=10, network_savename=None, condition_var=None):
        cvae = self.cvae()

        saver = tf.train.Saver()
        sample_xs = []
        n_sample_xs = 0

        if condition_var is None:
            condition_var = np.ones([mbsize,1])
        else:
            condition_var = np.array(condition_var).reshape(-1,self.params['n_conditions'])
            assert condition_var.shape[0] == n, \
                   'Mismatch number of conditional variables and features!'

        with tf.Session(config = self.tfconfig) as sess:
            sess.run(tf.global_variables_initializer())

            if network_savename is not None:
                saver.restore(sess, network_savename)

            while n_sample_xs < n:
                sample_x = sess.run(cvae['sample_x'], 
                              feed_dict={cvae['is_training']: False, 
                                         cvae['z']: np.random.randn(mbsize,self.params['latent_dim']), 
                                         cvae['c']: condition_var})

                sample_xs.append( sample_x )
                n_sample_xs += sample_x.shape[0]

        sample_xs = np.concatenate(sample_xs, axis=0)

        return sample_xs[:n,:]

    
    def get_errorloss_and_gradient(self, X, Y=None, mbsize=1, n_samples=10, network_savename=None):
        # requires
        #     X: feature
        #     Y: conditional variable
        # returns
        #     reconsruction loss
        #     KL loss
        #     VAE loss
        #     gradient of the VAE loss w.r.t the input
        if Y is None:
            assert self.params['n_conditions'] == 0, 'The conditional variable must be provided with dimension: {}'.format(self.params['n_conditions'])

            Y = np.ones([X.shape[0], 1])

        gvae_all = np.zeros(X.shape)
        reconloss_all = np.zeros([X.shape[0], 1])
        klloss_all = np.zeros([X.shape[0], 1])

        saver = tf.train.Saver()
        cvae = self.cvae()

        with tf.Session(config = self.tfconfig) as sess:
            sess.run(tf.global_variables_initializer())

            if network_savename is not None:
                saver.restore(sess, network_savename)

            n_samples = int(n_samples / self.params['nz_samples'])+1
            
            for i in range(0, X.shape[0], mbsize):

                for xx in range(n_samples):
                    recon_loss, kl_loss, gvae \
                        = sess.run([cvae['recon_loss'], 
                                    cvae['kl_loss'],
                                    cvae['grad_vaeloss_wrt_x']], 
                            feed_dict={cvae['X']: X[i:mbsize,:], 
                                       cvae['c']: Y[i:mbsize,:], 
                                       cvae['is_training']:False})

                    reconloss_all[i:mbsize,:] += recon_loss.reshape(-1,1) / float(n_samples)
                    klloss_all[i:mbsize,:] += kl_loss.reshape(-1,1) / float(n_samples)
                    gvae_all[i:mbsize,:] += gvae / float(n_samples)

        return reconloss_all, klloss_all, gvae_all

