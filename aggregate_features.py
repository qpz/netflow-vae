import flow_io
import argparse
import timeit
import sys
from joblib import Parallel, delayed
import random
import os.path
import pandas as pd
import numpy as np

from config import IP_DIM_DESTINATION, IP_DIM_SOURCE, col_headers
from features_definition import all_features




def process_ips_one_direction(df, feature_names):
    """Compute the aggregated features specified by feature_names
    in dataframe df (containing flows for 1 IP only, in 1 time window)

    Arguments:
    df            -- the dataframe containing flows in a time
                     window for an ip
    feature_names -- list of [feature_name, feature_type]
                     e.g, ['n_flow', IP_DIM_SOURCE]

    Returns: a series of aggregated features
    """
    se_ret = pd.Series()

    df = df.copy()

    for feature_name, feature_type in feature_names:
        new_feat = all_features[feature_name][feature_type](df)

        if all_features[feature_name]['type'] == 'onehot':
            # feature consists of a vector of values
            se_ret = pd.concat([se_ret, new_feat])
        else:
            # feature consists of a scalar value
            se_ret[ feature_name ] = new_feat

    return se_ret


def get_aggregate_features(df, feature_names, max_n_sample_ips, min_n_flow, n_jobs):
    """Compute the aggregated features specified by feature_names
    in dataframe df (containing flow for many IPs, in 1 time window)

    Arguments:
    df            -- the dataframe containing flows in a time
                     window for an ip
    feature_names -- list of [feature_name, feature_type]
                     e.g, ['n_flow', IP_DIM_SOURCE]
    max_n_sample_ips -- as defined in sampling_ip_params (config.py)
    min_n_flow       -- as defined in sampling_ip_params (config.py)
    n_jobs           -- as defined in computing_speed_params (config.py)

    Returns: 
    A dataframe of aggregated features for all IPs in the time window
    """
    start_time_0 = timeit.default_timer()

    df.loc[:,IP_DIM_SOURCE] = df['src_ip']
    df.loc[:,IP_DIM_DESTINATION] = df['dst_ip']


    feature_types = feature_names[:,1]
    assert not (IP_DIM_DESTINATION in feature_types and IP_DIM_SOURCE in feature_types), \
           'Haven\'t implement for mixed feature types'
    feature_type = feature_names[0,1]


    g_obj = df.groupby(feature_type)
    g_obj_count = g_obj.size() # Series
    ips_with_sufficient_flows = g_obj_count[ g_obj_count >= min_n_flow ].index.values

    if len(ips_with_sufficient_flows) == 0:
        return pd.DataFrame()

    if len(ips_with_sufficient_flows) > max_n_sample_ips:
        sampled_ips = random.sample(list(ips_with_sufficient_flows), 
                                    max_n_sample_ips)
    else:
        sampled_ips = ips_with_sufficient_flows

    print("    Sampled IPs / Filtered IPs / Total IPs = {} / {} / {}".format(
          len(sampled_ips), len(ips_with_sufficient_flows), len(g_obj_count)))

    df_agg_feats = Parallel(n_jobs=n_jobs)( 
                     delayed(process_ips_one_direction)(
                                   g_obj.get_group(ip), 
                                   feature_names) 
                     for ip in sampled_ips
                   )

    df_agg_feats = pd.concat(df_agg_feats, axis=1, sort=True).T
    df_agg_feats = df_agg_feats.fillna(0.0)

    elapsed = timeit.default_timer() - start_time_0
    print('    extract aggregate features: %.4fs' % (elapsed)) 
    sys.stdout.flush()

    return df_agg_feats



def get_next_batch(paths, feature_names, computing_speed_params, 
                   aggregate_ipwise_feature_params, 
                   ip_time_params, sampling_ip_params):
    """return minibatch of aggregated features of time windows
    specified by aggregate_ip_wise_feature_params
    The aggregated features are computed from the netflow 
    data (csv) in paths.

    Arguments:
    paths            -- list of paths to netflow data files (csv)
                        without header row, or index column.
                        Its column headers, and types are specified in 
                        col_headers, col_types (config.py)
    feature_names    -- list of [feature_name, feature_type]
                        e.g, ['n_flow', IP_DIM_SOURCE]
    *_params         -- as specified in config.py

    Returns iterator of dataframes containing the aggregated features
    """
    n_jobs = computing_speed_params['n_jobs']
    min_n_flows =  sampling_ip_params['min_n_flows']
    max_n_sample_ips = sampling_ip_params['max_n_sample_ips']
    n_minute_step = aggregate_ipwise_feature_params['minute_steps']
    n_minutes = aggregate_ipwise_feature_params['n_minutes']

    random.seed(1)
    np.random.seed(1)

    yielded = False

    for xx,path in enumerate(paths):

        print('\n================================')
        print('***Reading features per IP from {}'.format(path)); sys.stdout.flush()

        print("Read per %d minute%s" % (n_minutes, ("" if n_minutes == 1 else "s")))

        dfs = flow_io.read_per_minute(path, chunksize=5*10**5, 
                    start_inclusive=ip_time_params['start_min_inclusive'], 
                    end_inclusive=ip_time_params['end_min_inclusive'])
        minute_buffer = [] # to store all flows within a time window

        for timestamp, df in dfs:

            if timestamp == -1:
                # no valid indices found
                print("No valid flow in the time window of size {} from {} minute to {} minute in {}".format(
                            aggregate_ipwise_feature_params['n_minutes'],
                            ip_time_params['start_min_inclusive'], 
                            ip_time_params['end_min_inclusive'], 
                            path))
                continue

            # only process the data within minute/hour specified in timerange
            elif timestamp < ip_time_params['start_min_inclusive']:
                continue
            elif timestamp > ip_time_params['end_min_inclusive']:
                break
            
            minute_buffer.append(df.copy())

            if len(minute_buffer) < n_minutes: 
                # continue to read until the time window is filled
                # NOTE: if len(dfs) = 7, n_minute = 5, we ignore the last 2 minutes
                continue
            else:
                df = pd.concat(minute_buffer)

                # clear the minute_buffer
                for _ in range(n_minute_step):
                    minute_buffer.pop(0)
                if df.size == 0:
                    continue
                
            # Extract aggregate features from the time window data
            print('  %d. Processing the data.' % timestamp)
            sys.stdout.flush()
            
            df_agg_feats = get_aggregate_features(df, feature_names, 
                                                  max_n_sample_ips, 
                                                  min_n_flows, n_jobs)
            if df_agg_feats.shape[0] == 0:
                continue # no data received

            yielded = True
            yield df_agg_feats 

    if not yielded:
        yield pd.DataFrame()


