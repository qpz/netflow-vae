import pandas as pd
import itertools
import sys

from config import col_headers, col_types



def read_per_hour(filename, start_inclusive=0, end_inclusive=23, chunksize=10**5):
    ''' Wrapper to read and return data per-hour which is
    split into per-hour blocks over 24 hours
    *** NOTE *** This returns a pandas view, not a dataframe
    @return iterator of dataframes for data per hour
    @param filename, assumed to contain data for exactly one day
    '''

    chunks = read_csv(filename, chunksize=chunksize)

    compute_index = lambda x: x['time'].apply(
                              lambda z: int(z.split(':')[0]) 
                              ) 
    # index starts from start, ends at end
    dfs = iter_valid(chunks, compute_index, start_inclusive, end_inclusive)

    return dfs


def read_per_minute(filename, start_inclusive=0, end_inclusive=(60*24-1), 
                    chunksize=10**5):
    ''' Wrapper to read and return data per-minute which is
    split into per-minute blocks over 24 hours
    @return iterator of dataframes for data per minute
    *** NOTE *** This returns a pandas view, not a dataframe
    @param filename, assumed to contain data for exactly one day
    '''

    chunks = read_csv(filename, chunksize=chunksize)

    def GetMinutes(s): # Convert HH:MM into minutes
        # z = list(map(int, s.split(':')))
        # return int(z[0] * 60 + z[1])
        z = s.split(':')
        return int(z[0]) * 60 + int(z[1])

    compute_index = lambda x: x['time'].apply( lambda z: GetMinutes(z) )

    # index starts from start, ends at end
    dfs = iter_valid(chunks, compute_index, start_inclusive, end_inclusive)

    return dfs


def read_csv(filename, chunksize=10**5):
    ''' Read chunks from filename
    @return chunks with specified chunksize
    '''
    # Read chunks
    chunks = pd.read_csv(filename,
                        header=0,
                        names=col_headers,
                        dtype=dict(zip(col_headers, col_types)),
                        chunksize=chunksize,
                        iterator=True)

    return chunks


def iter_valid(chunks, compute_index, start_inclusive, end_inclusive):
    '''
    @return iterator of dataframes view given iterator of chunks
    ***NOTE*** This would return a view, not a copy. You will
    be required to create a copy of the dataframe if you want
    to edit columns as with a typical pandas dataframe.

    @param chunks
        - Iterator of chunks, of unknown size
    @param compute_index
        - Function compute index of each row in chunk
        - Assuming that index is in increasing order
        e.g., index: the minute of the row
    '''

    yielded = False
    time_window_outside_data = False
    df_per_index = pd.DataFrame()
    leftover = pd.DataFrame()
    leftindices = pd.Series()

    for i in range(start_inclusive, end_inclusive):
        if time_window_outside_data:
            break
 
        if leftover.size > 0:
            if leftindices.iloc[-1] < i:
                continue

            df_in_current_i = leftover[ leftindices == i ]
            df_more_than_i = leftover[ leftindices > i ]
           
            df_per_index = df_per_index.append( df_in_current_i )

            if df_more_than_i.size > 0:
                leftover = df_more_than_i
                leftindices = leftindices[ leftindices > i ]
                yielded = True
                yield i, df_per_index
                df_per_index = pd.DataFrame()
                continue
         

        for chunk in chunks:
            indices = compute_index(chunk)

            if indices.iloc[0] > end_inclusive:
                time_window_outside_data = True
                break
            else:
                df_in_current_i = chunk[indices == i]
                df_more_than_i = chunk[indices > i]

                df_per_index = df_per_index.append( df_in_current_i )

                if df_more_than_i.size > 0:
                    leftover = df_more_than_i
                    leftindices = indices[ indices > i ]
                    yielded = True
                    yield i, df_per_index
                    df_per_index = pd.DataFrame()
                    break


    if not yielded:
        if df_per_index.size > 0:
            indices = compute_index(df_per_index)
            yield indices.iloc[0], df_per_index
        else:
            # read all data but no valid index found
            yield -1, df_per_index


