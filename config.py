import numpy as np 



IP_DIM_DESTINATION = 'dst_ip_full'
IP_DIM_SOURCE = 'src_ip_full'


# UGR
"""col_headers: column names of the netflow csv file"""
col_headers = ['date', 'time', 'duration', 'src_ip', 
               'dst_ip', 'sport', 'dport', 
               'protocol', 'flag', 'fwd_status', 
               'type_of_service', 'packets', 'bytes', 
               'packets_rate', 'bytes_rate', 'label'] # fwd_status is useless (all are zero)

"""col_types: types of all columns in the netflow csv file"""
col_types = [str, str, float, str, 
            str, int, int, 
            str, str, int, 
            int, float, float,
            float, float, str]


"""
dictionary: feature_id -> list of features
  each feature is specified by: [feature_name, direction]
    e.g., feature_name = 'n_flow_': number of flows per aggregated record
          direction = IP_DIM_SOURCE: aggregating flows by the source ip
          direction = IP_DIM_DESTINATION: aggregating flows by the destination ip
    the definition of features are in all_features in features_definition.py
          {feature_name: feature definition dictionary}
"""
all_feature_set_ids = {'detect_smtp_anomaly': np.array([
                                                      ['ip', IP_DIM_SOURCE],
                                                      ['date', IP_DIM_SOURCE],
                                                      ['time', IP_DIM_SOURCE],
                                                      ['n_flow_', IP_DIM_SOURCE],
                                                      
                                                      ['avg_dur', IP_DIM_SOURCE],
                                                      ['avg_pkt', IP_DIM_SOURCE],
                                                      ['avg_bytes', IP_DIM_SOURCE],

                                                      ['std_dur', IP_DIM_SOURCE],
                                                      ['std_pkt', IP_DIM_SOURCE],
                                                      ['std_bytes', IP_DIM_SOURCE],

                                                      ['avg_pkt_over_dur', IP_DIM_SOURCE],
                                                      ['avg_bytes_over_dur', IP_DIM_SOURCE],
                                                      ['std_pkt_over_dur', IP_DIM_SOURCE],
                                                      ['std_bytes_over_dur', IP_DIM_SOURCE],

                                                      ['n_unique_srcip_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_dstip_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_sport_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_dport_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_flag_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_typeofservice_over_nflow', IP_DIM_SOURCE], # string cut off by np array
                                                      
                                                      ['entropy_srcip_', IP_DIM_SOURCE],
                                                      ['entropy_dstip_', IP_DIM_SOURCE],
                                                      ['entropy_sport_', IP_DIM_SOURCE],
                                                      ['entropy_dport_', IP_DIM_SOURCE],
                                                      ['entropy_protocol_', IP_DIM_SOURCE],
                                                      ['entropy_flag_', IP_DIM_SOURCE],
                                                      ['entropy_typeofservice_', IP_DIM_SOURCE],

                                                      ['mean_srcip_count_', IP_DIM_SOURCE],
                                                      ['mean_dstip_count_', IP_DIM_SOURCE],
                                                      ['mean_sport_count_', IP_DIM_SOURCE],
                                                      ['mean_dport_count_', IP_DIM_SOURCE],
                                                      ['mean_protocol_count_', IP_DIM_SOURCE],
                                                      ['mean_flag_count_', IP_DIM_SOURCE],
                                                      ['mean_typeofservice_count_', IP_DIM_SOURCE],

                                                      ['std_srcip_count_', IP_DIM_SOURCE],
                                                      ['std_dstip_count_', IP_DIM_SOURCE],
                                                      ['std_sport_count_', IP_DIM_SOURCE],
                                                      ['std_dport_count_', IP_DIM_SOURCE],
                                                      ['std_protocol_count_', IP_DIM_SOURCE],
                                                      ['std_flag_count_', IP_DIM_SOURCE],
                                                      ['std_typeofservice_count_', IP_DIM_SOURCE],

                                                      ['avg_arr_time_interval_', IP_DIM_SOURCE],
                                                      ['std_arr_time_interval_', IP_DIM_SOURCE],
                                                      ['is_dport_80', IP_DIM_SOURCE],
                                                      ['is_sport_80', IP_DIM_SOURCE],
                                                      ['is_dport_lt1000', IP_DIM_SOURCE],
                                                      ['is_sport_lt1000', IP_DIM_SOURCE],
                                                      ['is_dport_smtp', IP_DIM_SOURCE],
                                                      ['is_sport_smtp', IP_DIM_SOURCE],
                                                      ['is_9to5', IP_DIM_SOURCE],
                                                      # ['onehot_flag', IP_DIM_SOURCE],
                                                      # !!! for onehot feature: name must be '{}_{}'.format('onehot', featname)
                                                      ['onehot_protocol', IP_DIM_SOURCE],
                                                      ['onehot_label', IP_DIM_SOURCE],
                                                 ], dtype=object),
                          'port_distribution': np.array([
                                                      ['ip', IP_DIM_SOURCE],
                                                      ['date', IP_DIM_SOURCE],
                                                      ['time', IP_DIM_SOURCE],
                                                      ['n_flow_', IP_DIM_SOURCE],
                                                      
                                                      ['avg_dur', IP_DIM_SOURCE],
                                                      ['avg_pkt', IP_DIM_SOURCE],
                                                      ['avg_bytes', IP_DIM_SOURCE],

                                                      ['std_dur', IP_DIM_SOURCE],
                                                      ['std_pkt', IP_DIM_SOURCE],
                                                      ['std_bytes', IP_DIM_SOURCE],

                                                      ['avg_pkt_over_dur', IP_DIM_SOURCE],
                                                      ['avg_bytes_over_dur', IP_DIM_SOURCE],
                                                      ['std_pkt_over_dur', IP_DIM_SOURCE],
                                                      ['std_bytes_over_dur', IP_DIM_SOURCE],

                                                      ['n_unique_srcip_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_dstip_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_sport_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_dport_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_flag_over_nflow', IP_DIM_SOURCE],
                                                      ['n_unique_typeofservice_over_nflow', IP_DIM_SOURCE], # string cut off by np array
                                                      
                                                      ['entropy_srcip_', IP_DIM_SOURCE],
                                                      ['entropy_dstip_', IP_DIM_SOURCE],
                                                      ['entropy_sport_', IP_DIM_SOURCE],
                                                      ['entropy_dport_', IP_DIM_SOURCE],
                                                      ['entropy_protocol_', IP_DIM_SOURCE],
                                                      ['entropy_flag_', IP_DIM_SOURCE],
                                                      ['entropy_typeofservice_', IP_DIM_SOURCE],

                                                      ['mean_srcip_count_', IP_DIM_SOURCE],
                                                      ['mean_dstip_count_', IP_DIM_SOURCE],
                                                      ['mean_sport_count_', IP_DIM_SOURCE],
                                                      ['mean_dport_count_', IP_DIM_SOURCE],
                                                      ['mean_protocol_count_', IP_DIM_SOURCE],
                                                      ['mean_flag_count_', IP_DIM_SOURCE],
                                                      ['mean_typeofservice_count_', IP_DIM_SOURCE],

                                                      ['std_srcip_count_', IP_DIM_SOURCE],
                                                      ['std_dstip_count_', IP_DIM_SOURCE],
                                                      ['std_sport_count_', IP_DIM_SOURCE],
                                                      ['std_dport_count_', IP_DIM_SOURCE],
                                                      ['std_protocol_count_', IP_DIM_SOURCE],
                                                      ['std_flag_count_', IP_DIM_SOURCE],
                                                      ['std_typeofservice_count_', IP_DIM_SOURCE],

                                                      ['avg_arr_time_interval_', IP_DIM_SOURCE],
                                                      ['std_arr_time_interval_', IP_DIM_SOURCE],
                                                      ['is_dport_80', IP_DIM_SOURCE],
                                                      ['is_sport_80', IP_DIM_SOURCE],
                                                      ['is_dport_lt1000', IP_DIM_SOURCE],
                                                      ['is_sport_lt1000', IP_DIM_SOURCE],
                                                      ['is_dport_smtp', IP_DIM_SOURCE],
                                                      ['is_sport_smtp', IP_DIM_SOURCE],

                                                      ['is_dport_ftp', IP_DIM_SOURCE], 
                                                      ['is_sport_ftp', IP_DIM_SOURCE], 
                                                      ['is_dport_ssh', IP_DIM_SOURCE], 
                                                      ['is_sport_ssh', IP_DIM_SOURCE], 
                                                      ['is_dport_telnet', IP_DIM_SOURCE], 
                                                      ['is_sport_telnet', IP_DIM_SOURCE], 
                                                      ['is_dport_dns', IP_DIM_SOURCE], 
                                                      ['is_sport_dns', IP_DIM_SOURCE], 
                                                      ['is_dport_http', IP_DIM_SOURCE], 
                                                      ['is_sport_http', IP_DIM_SOURCE], 
                                                      ['is_dport_pop3', IP_DIM_SOURCE], 
                                                      ['is_sport_pop3', IP_DIM_SOURCE], 
                                                      ['is_dport_winrpc', IP_DIM_SOURCE], 
                                                      ['is_sport_winrpc', IP_DIM_SOURCE], 
                                                      ['is_dport_winnetbios', IP_DIM_SOURCE], 
                                                      ['is_sport_winnetbios', IP_DIM_SOURCE], 
                                                      ['is_dport_mssqlserver', IP_DIM_SOURCE], 
                                                      ['is_sport_mssqlserver', IP_DIM_SOURCE],
                                                      
                                                      ['is_9to5', IP_DIM_SOURCE],
                                                      # ['onehot_flag', IP_DIM_SOURCE],
                                                      # !!! for onehot feature: name must be '{}_{}'.format('onehot', featname)
                                                      ['onehot_protocol', IP_DIM_SOURCE],
                                                      ['onehot_label', IP_DIM_SOURCE],
                                                 ], dtype=object),
                           'dport1000_distribution': np.array([ 
                                                      ['onehot_dport1000', IP_DIM_SOURCE]
                                                 ], dtype=object)}


"""features excluding from training"""
config_excluded_feat_cols=['n_flow_', 'entropy_typeofservice_', 'mean_srcip_count_', 'mean_dstip_count_', 
                        'mean_sport_count_', 'mean_dport_count_', 
                        'mean_protocol_count_', 'mean_flag_count_', 
                        'mean_typeofservice_count_', 'std_srcip_count_', 
                        'std_dstip_count_', 'std_sport_count_', 
                        'std_dport_count_', 'std_protocol_count_', 
                        'std_flag_count_', 'std_typeofservice_count_', 
                        'avg_arr_time_interval_', 'std_arr_time_interval_', 'is_9to5', 'is_july30']

""""feature to condition on in the Conditional Variational Autoencoder"""
config_cond_cols = []#['is_july30']


"""n_jobs -- number of jobs to extract features"""
computing_speed_params = {'n_jobs': 16}

"""
n_minutes    -- the time window size to aggregate features
minute_steps -- the step to slide the window each iteration
e.g., with n_minutes = 3, minute_steps = 2, the time windows are
    [0,3], [2,5], [4,7], ...
"""
aggregate_ipwise_feature_params = {'n_minutes': 3, 
                                   'minute_steps': 1}


"""
min_n_flows      -- ips with no. of flows <= min_n_flows are not used
                    to compute the aggregated features
max_n_sample_ips -- in a time window, max_n_sample_ips is the maximum 
                    number of ips to compute the aggregated features
"""
sampling_ip_params = {'min_n_flows': 10000,
                      'max_n_sample_ips': 100000}

"""
start_min_inclusive -- the start of the time period to compute the features
end_min_inclusive   -- the end of the time period to compute the features
"""
ip_time_params = {'start_min_inclusive': 0,
                  'end_min_inclusive': 5}


"""CVAE structure"""
vae_params = {'latent_dim': 100,
              # no. of hidden neurons for n_hidden_layer layers
              'hidden_neurons': [512, 512, 1024], 
              'n_hidden_layer': 3, # == len(hidden_neurons)
              # batch size for stochastic gradient optimization
              'batch_size': 500, 
              # no. of latent samples to evaluate the objective function of CVAE
              'nz_samples': 100} 

