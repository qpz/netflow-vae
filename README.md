# Anomaly Detection in Netflow data with VAE

The project is about using Variational Autoencoder (VAE) to detect anomaly in the netflow data.
It includes 2 parts:

1. Compute aggregated features from the netflow data.

2. Training and testing the aggregated features with VAE.

## Prerequisite

Packages required:

```
python=3.*
numpy
pandas>=0.23.3
tensorflow
matplotlib
joblib
```
