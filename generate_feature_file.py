import numpy as np
import pandas as pd 
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import sys

from config import *
from features_definition import all_features
from aggregate_features import get_next_batch


def rearrange_feature_columns(feature_names, df_cols):
    """Re-arraynge the list of column names
    such that the information feature names (e.g., ip, date, time) 
    are at the beginning of the list, and the feature names of 
    an one-hot feature are close to each other

    Arguments:
    feature_names -- list of feature names
    df_cols       -- list of column names

    Returns: the re-arranged column name list
    """
    rearrange_cols = []

    for feat in list(feature_names[:,0].squeeze()):
        if all_features[feat]['type'] == 'information':
            # e.g., date, time, ip
            rearrange_cols.append(feat)

        elif all_features[feat]['type'] == 'onehot':
            for c in df_cols:
                if all_features[feat]['is_feature_name'](c):
                    rearrange_cols.append(c)

        else:
            for c in df_cols:
                if c == feat:
                    rearrange_cols.append(c)


    if len(rearrange_cols) != len(df_cols):
        print("WARNING: no. of features != no. of columns, {} vs. {}".format(len(rearrange_cols), len(df_cols)))
        print("         The difference: {}".format(set(df_cols) - set(rearrange_cols))); print(df_cols); print(rearrange_cols);
        print("                         {}".format(set(rearrange_cols) - set(df_cols)))

    return rearrange_cols



def generate_feature_file(paths, feature_id, write_path='.', feature_filename='features.csv'):
    """Writing a csv file containing the aggregated features 
    to the location write_path/feature_filename. 
    The aggregated features are computed from the netflow 
    data (csv) in paths.

    Arguments:
    paths            -- list of paths to netflow data files (csv)
                        without header row, or index column.
                        Its column headers, and types are specified in 
                        col_headers, col_types (config.py)
    feature_id       -- string id to identify the set of features,
                        which is specified in all_feature_set_ids (config.py)
    write_path       -- path to write the feature file
    feature_filename -- filename of the feature file (.csv)
    """
    feature_names = all_feature_set_ids[feature_id]

    batches = get_next_batch(paths, feature_names, computing_speed_params, 
                    aggregate_ipwise_feature_params, ip_time_params, sampling_ip_params)


    df_all = []

    for df in batches:
        df_all.append(df)

    df_all = pd.concat(df_all, axis=0, sort=True); df_all = df_all.fillna(0.0)
    df_cols = list(df_all.columns.values)

    if df_all.size == 0:
        print("No valid data read!")
        return

    new_columns = rearrange_feature_columns(feature_names, df_cols)
    df_all = df_all[new_columns]

    if not os.path.isdir(write_path):
        os.makedirs(write_path)

    df_all.to_csv(write_path + '/' + feature_filename)

