import numpy as np
import pandas as pd
import timeit
import sys

from config import *


"""List of possible values for some feature
to compute one-hot features
    e.g., values of 'protocol' in { 'ICMP', 'IPv6', 'TCP', 'UDP' }
    convered to one-hot features:
    'protocol:ICMP', 'protocol:IPv6', 'protocol:TCP', 'protocol:UDP'
    where each feature is the frequency/proportion of that value
    appearing the time window.
"""
port1000_featvals = set(range(1,1000))
flag_featvals = set(['......', '....S.', '...R..', '...RS.', '.A....', '.A...F', '.A..S.', '.A..SF', '.A.R..', '.A.R.F', '.A.RS.', '.A.RSF', '.AP...', '.AP..F', '.AP.S.', '.AP.SF', '.APR..', '.APR.F', '.APRS.', '.APRSF', 'UA...F', 'UA..SF'])
protocol_featvals = set(['ESP', 'GRE', 'ICMP', 'IPv6', 'TCP', 'UDP'])
label_featvals = set(['anomaly-spam', 'background', 'blacklist'])



"""Functions to compute aggregated features

Arguments:
df -- the dataframes containing flows in a time window
"""
def cal_avg_dur(df):
    return df['duration'].mean()

def cal_avg_pkt(df):
    return df['packets'].mean()

def cal_avg_bytes(df):
    return df['bytes'].mean()

def cal_std_dur(df):
    return df['duration'].std()

def cal_std_pkt(df):
    return df['packets'].std()

def cal_std_bytes(df):
    return df['bytes'].std()


def cal_avg_pkt_over_dur(df):
    return df['packets_rate'].mean()

def cal_avg_bytes_over_dur(df):
    return df['bytes_rate'].mean()

def cal_std_pkt_over_dur(df):
    return df['packets_rate'].std()

def cal_std_bytes_over_dur(df):
    return df['bytes_rate'].std()


## Entropy features
def entropy(probs):
    probs = np.array([p for p in probs if p > 0.0])
    return - np.sum(probs * np.log(probs))

def extract_proportion_unique(df, feature_name):
    df['one'] = 1.0
    tmp = df.groupby(feature_name).count()['one']
    v = tmp.values.astype(float)
    v = v / np.sum(v)
    return v


def extract_proportion_mean(df, feature_name):
    df['one'] = 1.0
    tmp = df.groupby(feature_name).count()['one']
    v = tmp.values.astype(float)
    return v.mean()


def extract_proportion_std(df, feature_name):
    df['one'] = 1.0
    tmp = df.groupby(feature_name).count()['one']
    v = tmp.values.astype(float)
    return v.std()


# Proto, Sport, Dport, State, 
def cal_n_unique_source_ips_over_n_flows(df):
    n_unique_ips = df[IP_DIM_SOURCE].nunique()
    return float(n_unique_ips) / len(df)

def cal_n_unique_destination_ips_over_n_flows(df):
    n_unique_ips = df[IP_DIM_DESTINATION].nunique()
    return float(n_unique_ips) / len(df)

def cal_n_unique_source_ports_over_n_flows(df):
    n_unique_ips = df['sport'].nunique()
    return float(n_unique_ips) / len(df)

def cal_n_unique_destination_ports_over_n_flows(df):
    n_unique_ips = df['dport'].nunique()
    return float(n_unique_ips) / len(df)

def cal_n_unique_flags_over_n_flows(df):
    n_unique_ips = df['flag'].nunique()
    return float(n_unique_ips) / len(df)

def cal_n_unique_type_of_services_over_n_flows(df):
    n_unique_ips = df['type_of_service'].nunique()
    return float(n_unique_ips) / len(df)


def cal_entropy(df, feat_name):
    # feat_name = IP_DIM_SOURCE, sport, dport, protocol, flag, type_of_service
    v = extract_proportion_unique(df, feat_name)
    return entropy(v)


# one-hot features
def cal_avg_onehot(df, feat_name, featvals=None):
    # featvals: set of possible values for the feature with feat_name
    # NOTE: featvals is a set
    df1 = pd.get_dummies(df[feat_name])

    if featvals is not None:
        # extend the dataframe with other feature values
        # which may not appear in df
        dfvals = set(df1.columns.values)
        vals = featvals.intersection(dfvals)
        df1 = df1[list(vals)]
        remainvals = featvals - vals
        zerodf = pd.DataFrame(0, index=df1.index, columns=list(remainvals))
        df1 = pd.concat([df1, zerodf], axis=1)

    df1.columns = ['{}:{}'.format(feat_name,n) for n in df1.columns.values]
    return df1.mean()


def get_n_flow(df):
    return len(df)


# statistics about arriving time interval
def cal_avg_arr_time_interval(df):
    def get_seconds(s): # Convert HH:MM into minutes
        # z = list(map(int, s.split(':')))
        # return int(z[0] * 60 + z[1])
        z = s.split(':')
        return float(z[0]) * 3600 + float(z[1]) * 60 + float(z[2])

    if 'seconds' not in df.columns.values:
        df['seconds'] = df['time'].apply(lambda x: get_seconds(x))
    
    return df['seconds'].diff().mean(skipna=True)


def cal_std_arr_time_interval(df):
    def get_seconds(s): # Convert HH:MM into minutes
        # z = list(map(int, s.split(':')))
        # return int(z[0] * 60 + z[1])
        z = s.split(':')
        return float(z[0]) * 3600 + float(z[1]) * 60 + float(z[2])

    if 'seconds' not in df.columns.values:
        df['seconds'] = df['time'].apply(lambda x: get_seconds(x))
    
    return df['seconds'].diff().std(skipna=True)


# statistics about condition on continuous random variable (or int)
# as a counterpart for onehot for categorial random variables
def cal_true_proportion(df, featname, condition):
    # for example is_port_80
    #    cal_true_proportion(df, 'dport', (lambda x: x==80))
    return df[featname].apply(lambda x: condition(x)).astype(float).mean()



"""the definition of all features

The definition of a feature includes:
IP_DIM_SOURCE      -- function F to compute feature 
                      if aggregating flows for each source IP
IP_DIM_DESTINATION -- function F to compute feature
                      if aggregating flows for each destination IP
type               -- can be either 'information', 'scalar', or 'onehot'
                      information: feature used to indentify
                                   (e.g., ip, date, time)
                      scalar: F returns a scalar
                      onehot: F returns a series (pandas) 
bidir_type         -- can be either 'sum', 'concat'
                      used for bi-directional aggregated features
                      haven't implemented
                      sum:    sum the features of the 2 directions
                      concat: concat the features of the 2 directions
"""
all_features = {'date': {IP_DIM_SOURCE: (lambda df:  df['date'].iloc[0]),
                         IP_DIM_DESTINATION: (lambda df: df['date'].iloc[0]),
                         'type': 'information'},
                'time': {IP_DIM_SOURCE: (lambda df: df['time'].iloc[0]),
                         IP_DIM_DESTINATION: (lambda df: df['time'].iloc[0]),
                         'type': 'information'},
                'ip': {IP_DIM_SOURCE: (lambda df: df[IP_DIM_SOURCE].iloc[0]),
                       IP_DIM_DESTINATION: (lambda df: df[IP_DIM_DESTINATION].iloc[0]),
                       'type': 'information'},
                'n_flow_': {IP_DIM_SOURCE: get_n_flow,
                           IP_DIM_DESTINATION: get_n_flow,
                           'type': 'scalar', # scalar, onehot
                           'bidir_type': 'concat' # concat, sum
                           },
                'avg_dur': {IP_DIM_SOURCE: cal_avg_dur,
                            IP_DIM_DESTINATION: cal_avg_dur,
                            'type': 'scalar',
                            'bidir_type': 'concat'
                            },
                'avg_pkt': {IP_DIM_SOURCE: cal_avg_pkt,
                            IP_DIM_DESTINATION: cal_avg_pkt,
                            'type': 'scalar',
                            'bidir_type': 'concat'
                            },
                'avg_bytes': {IP_DIM_SOURCE: cal_avg_bytes,
                              IP_DIM_DESTINATION: cal_avg_bytes,
                              'type': 'scalar',
                              'bidir_type': 'concat'
                              },
                'std_dur': {IP_DIM_SOURCE: cal_std_dur,
                            IP_DIM_DESTINATION: cal_std_dur,
                            'type': 'scalar',
                            'bidir_type': 'concat'
                            },
                'std_pkt': {IP_DIM_SOURCE: cal_std_pkt,
                            IP_DIM_DESTINATION: cal_std_pkt,
                            'type': 'scalar',
                            'bidir_type': 'concat'
                            },
                'std_bytes': {IP_DIM_SOURCE: cal_std_bytes,
                              IP_DIM_DESTINATION: cal_std_bytes,
                              'type': 'scalar',
                              'bidir_type': 'concat'
                              },
                'avg_pkt_over_dur': {IP_DIM_SOURCE: cal_avg_pkt_over_dur,
                                     IP_DIM_DESTINATION: cal_avg_pkt_over_dur,
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'avg_bytes_over_dur': {IP_DIM_SOURCE: cal_avg_bytes_over_dur,
                                       IP_DIM_DESTINATION: cal_avg_bytes_over_dur,
                                       'type': 'scalar',
                                       'bidir_type': 'concat'
                                       },
                'std_pkt_over_dur': {IP_DIM_SOURCE: cal_std_pkt_over_dur,
                                     IP_DIM_DESTINATION: cal_std_pkt_over_dur,
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_bytes_over_dur': {IP_DIM_SOURCE: cal_std_bytes_over_dur,
                                       IP_DIM_DESTINATION: cal_std_bytes_over_dur,
                                       'type': 'scalar',
                                       'bidir_type': 'concat'
                                       },
                'n_unique_srcip_over_nflow': {IP_DIM_SOURCE: cal_n_unique_source_ips_over_n_flows,
                                              IP_DIM_DESTINATION: cal_n_unique_source_ips_over_n_flows,
                                              'type': 'scalar',
                                              'bidir_type': 'concat'
                                              },
                'n_unique_dstip_over_nflow': {IP_DIM_SOURCE: cal_n_unique_destination_ips_over_n_flows,
                                              IP_DIM_DESTINATION: cal_n_unique_destination_ips_over_n_flows,
                                              'type': 'scalar',
                                              'bidir_type': 'concat'
                                              },
                'n_unique_sport_over_nflow': {IP_DIM_SOURCE: cal_n_unique_source_ports_over_n_flows,
                                              IP_DIM_DESTINATION: cal_n_unique_source_ips_over_n_flows,
                                              'type': 'scalar',
                                              'bidir_type': 'concat'
                                              },
                'n_unique_dport_over_nflow': {IP_DIM_SOURCE: cal_n_unique_destination_ports_over_n_flows,
                                              IP_DIM_DESTINATION: cal_n_unique_destination_ports_over_n_flows,
                                              'type': 'scalar',
                                              'bidir_type': 'concat'
                                              },
                'n_unique_flag_over_nflow': {IP_DIM_SOURCE: cal_n_unique_flags_over_n_flows,
                                             IP_DIM_DESTINATION: cal_n_unique_flags_over_n_flows,
                                             'type': 'scalar',
                                             'bidir_type': 'concat'
                                             },
                'n_unique_typeofservice_over_nflow': {IP_DIM_SOURCE: cal_n_unique_type_of_services_over_n_flows,
                                                      IP_DIM_DESTINATION: cal_n_unique_type_of_services_over_n_flows,
                                                      'type': 'scalar',
                                                      'bidir_type': 'concat'
                                                      },
                'entropy_srcip_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, IP_DIM_SOURCE)),
                                  IP_DIM_DESTINATION: (lambda x: cal_entropy(x, IP_DIM_SOURCE)),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'entropy_dstip_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, IP_DIM_DESTINATION)),
                                  IP_DIM_DESTINATION: (lambda x: cal_entropy(x, IP_DIM_DESTINATION)),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'entropy_sport_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, 'sport')),
                                  IP_DIM_DESTINATION: (lambda x: cal_entropy(x, 'sport')),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'entropy_dport_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, 'dport')),
                                  IP_DIM_DESTINATION: (lambda x: cal_entropy(x, 'sport')),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'entropy_protocol_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, 'protocol')),
                                     IP_DIM_DESTINATION: (lambda x: cal_entropy(x, 'protocol')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'entropy_flag_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, 'flag')),
                                 IP_DIM_DESTINATION: (lambda x: cal_entropy(x, 'flag')),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },

                'mean_srcip_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, IP_DIM_SOURCE)),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, IP_DIM_SOURCE)),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_dstip_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, IP_DIM_DESTINATION)),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, IP_DIM_DESTINATION)),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_sport_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, 'sport')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, 'sport')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_dport_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, 'dport')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, 'dport')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_protocol_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, 'protocol')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, 'protocol')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_flag_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, 'flag')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, 'flag')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'mean_typeofservice_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_mean(x, 'type_of_service')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_mean(x, 'type_of_service')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_srcip_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, IP_DIM_SOURCE)),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, IP_DIM_SOURCE)),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_dstip_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, IP_DIM_DESTINATION)),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, IP_DIM_DESTINATION)),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_sport_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, 'sport')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, 'sport')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_dport_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, 'dport')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, 'dport')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_protocol_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, 'protocol')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, 'protocol')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_flag_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, 'flag')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, 'flag')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'std_typeofservice_count_': {IP_DIM_SOURCE: (lambda x: extract_proportion_std(x, 'type_of_service')),
                                     IP_DIM_DESTINATION: (lambda x: extract_proportion_std(x, 'type_of_service')),
                                     'type': 'scalar',
                                     'bidir_type': 'concat'
                                     },
                'entropy_typeofservice_': {IP_DIM_SOURCE: (lambda x: cal_entropy(x, 'type_of_service')),
                                          IP_DIM_DESTINATION: (lambda x: cal_entropy(x, 'type_of_service')),
                                          'type': 'scalar',
                                          'bidir_type': 'concat'
                                          },
                'avg_arr_time_interval_': {IP_DIM_SOURCE: (lambda x: cal_avg_arr_time_interval(x)),
                                          IP_DIM_DESTINATION: (lambda x: cal_avg_arr_time_interval(x)),
                                          'type': 'scalar',
                                          'bidir_type': 'concat'
                                          },
                'std_arr_time_interval_': {IP_DIM_SOURCE: (lambda x: cal_std_arr_time_interval(x)),
                                          IP_DIM_DESTINATION: (lambda x: cal_std_arr_time_interval(x)),
                                          'type': 'scalar',
                                          'bidir_type': 'concat'
                                          },
                'is_dport_80': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y == 80) )),
                                IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y == 80) )),
                                'type': 'scalar',
                                'bidir_type': 'concat'
                                },
                'is_sport_80': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y == 80) )),
                                IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y == 80) )),
                                'type': 'scalar',
                                'bidir_type': 'concat'
                                },
                'is_dport_lt1000': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y < 1000) )),
                                IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y < 1000) )),
                                'type': 'scalar',
                                'bidir_type': 'concat'
                                },
                'is_sport_lt1000': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y < 1000) )),
                                IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y < 1000) )),
                                'type': 'scalar',
                                'bidir_type': 'concat'
                                },
                'is_dport_smtp': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y == 25) )), # [25, 465, 587, 2525]
                                  IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'sport', (lambda y: y == 25) )),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'is_sport_smtp': {IP_DIM_SOURCE: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y == 25) )),
                                  IP_DIM_DESTINATION: (lambda x: cal_true_proportion(x, 'dport', (lambda y: y == 25) )),
                                  'type': 'scalar',
                                  'bidir_type': 'concat'
                                  },
                'is_dport_ftp': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 21) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 21) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_ftp': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 21) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 21) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_ssh': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 22) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 22) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_ssh': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 22) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 22) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_telnet': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 23) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 23) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_telnet': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 23) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 23) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_dns': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 53) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 53) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_dns': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 53) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 53) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_http': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 443) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 443) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_http': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 443) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 443) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_pop3': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 110) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 110) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_pop3': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 110) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 110) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_winrpc': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 135) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y == 135) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_winrpc': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 135) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y == 135) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_winnetbios': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y in list(range(137,140)) ) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y in list(range(137,140)) ) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_winnetbios': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y in list(range(137,140)) ) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y in list(range(137,140)) ) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_dport_mssqlserver': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y in [1433, 1434]) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'dport', (lambda y: y in [1433, 1434]) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_sport_mssqlserver': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y in [1433, 1434]) )),
                                 IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'sport', (lambda y: y in [1433, 1434]) )),
                                 'type': 'scalar',
                                 'bidir_type': 'concat'
                                 },
                'is_9to5': {IP_DIM_SOURCE: (lambda x: cal_true_proportion( x, 'time', (lambda y: int(y.split(':')[0]) <= 17 and int(y.split(':')[0]) >= 9 ) )),
                            IP_DIM_DESTINATION: (lambda x: cal_true_proportion( x, 'time', (lambda y: int(y.split(':')[0]) <= 17 and int(y.split(':')[0]) >= 9 ) )),
                            'type': 'scalar',
                            'bidir_type': 'concat'
                            },
                'onehot_dport1000': {IP_DIM_SOURCE: (lambda x: cal_avg_onehot(x, 'dport', featvals=port1000_featvals)),
                                IP_DIM_DESTINATION: (lambda x: cal_avg_onehot(x, 'dport', featvals=port1000_featvals)),
                                'is_feature_name': (lambda feat: feat.startswith('dport:')),
                                'type': 'onehot',
                                'bidir_type': 'concat'
                                },
                'onehot_flag': {IP_DIM_SOURCE: (lambda x: cal_avg_onehot(x, 'flag', featvals=flag_featvals)),
                                IP_DIM_DESTINATION: (lambda x: cal_avg_onehot(x, 'flag', featvals=flag_featvals)),
                                'is_feature_name': (lambda feat: feat.startswith('flag:')),
                                'type': 'onehot',
                                'bidir_type': 'concat'
                                },
                'onehot_protocol': {IP_DIM_SOURCE: (lambda x: cal_avg_onehot(x, 'protocol', featvals=protocol_featvals)),
                                    IP_DIM_DESTINATION: (lambda x: cal_avg_onehot(x, 'protocol', featvals=protocol_featvals)),
                                    'is_feature_name': (lambda feat: feat.startswith('protocol:')),
                                    'type': 'onehot',
                                    'bidir_type': 'concat'
                                    },
                'onehot_label': {IP_DIM_SOURCE: (lambda x: cal_avg_onehot(x, 'label', featvals=label_featvals)),
                                 IP_DIM_DESTINATION: (lambda x: cal_avg_onehot(x, 'label', featvals=label_featvals)),
                                 'is_feature_name': (lambda feat: feat.startswith('label:')),
                                 'type': 'onehot',
                                 'bidir_type': 'sum'
                                 }
                }
